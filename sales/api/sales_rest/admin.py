from django.contrib import admin
from .models import AutomobileVO, Salesperson, Customer, Sale

class AutomobileVOAdmin(admin.ModelAdmin):
    pass

class SalespersonAdmin(admin.ModelAdmin):
    pass

class SaleAdmin(admin.ModelAdmin):
    pass

class CustomerAdmin(admin.ModelAdmin):
    pass

admin.site.register(AutomobileVO, AutomobileVOAdmin)
admin.site.register(Salesperson, SalespersonAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Sale, SaleAdmin)
