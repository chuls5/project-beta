from django.urls import path
from .views import (
    list_customers,
    customers_detail,
    list_salespeople,
    salespeople_detail,
    list_sales,
    sales_detail,
)

urlpatterns = [
    path("salespeople/", list_salespeople, name="list_salespeople"),
    path("salespeople/<int:pk>/", salespeople_detail, name="salespeople_detail"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:pk>/", customers_detail, name="customers_detail"),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:pk>/", sales_detail, name="sales_detail"),

]
