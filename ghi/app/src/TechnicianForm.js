import React from 'react';

class NewTechnicianForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            employeeId: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.showSuccessMessage = this.showSuccessMessage.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name: this.state.firstName,
            last_name: this.state.lastName,
            employee_id: this.state.employeeId
        };

        fetch("http://localhost:8080/api/technicians/", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        })
        .then(response => {
            if (response.ok) {
                this.setState({ first_name: "", last_name: "", employee_id: "" });
                this.showSuccessMessage();
            } else {
                alert("Did not add technician");
            }
        })
    }

    showSuccessMessage() {
        const successMessage = document.getElementById('success-message');
        successMessage.classList.remove("d-none");
        setTimeout(() => {
            successMessage.classList.add("d-none");
        }, 3000);
    }

    render() {
        return (
            <div className='container pt-5'>
                <div className='row'>
                    <div className='col-md-6 offset-md-3'>
                        <div className='shadow p-4 mt-4'>
                            <h1>Add a Technician</h1>
                            <form onSubmit={this.handleSubmit} id="create-technician-form">
                            <div className='form-group'>
    <input type="text" name="firstName" id="firstName"
           className="form-control" placeholder="First name..."
           value={this.state.firstName} onChange={this.handleInputChange}
           required />
</div>
<div className='form-group'>
    <input type="text" name="lastName" id="lastName"
           className="form-control" placeholder="Last name..."
           value={this.state.lastName} onChange={this.handleInputChange}
           required />
</div>
<div className='form-group'>
    <input type="number" name="employeeId" id="employeeId"
           className="form-control" placeholder="Employee ID..."
           value={this.state.employeeId} onChange={this.handleInputChange}
           required />
</div>
                                <button type='submit' className='btn btn-success'>Create</button>
                            </form>
                            <div className='alert alert-success d-none mt-5' id='success-message'>
                                Added a new technician!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default NewTechnicianForm;
