import { BrowserRouter, Routes, Route } from 'react-router-dom'
import MainPage from './MainPage'
import Nav from './Nav'
import VehicleModelsList from './VehicleModelsList'
import VehicleModelForm from './VehicleModelForm'
import AutomobileList from './AutomobileList'
import AutomobileForm from './AutomobileForm'
import ManufacturerList from './ManufacturerList'
import ManufacturerForm from './ManufacturerForm'


import CustomerForm from './CustomerForm'
import CustomerList from './CustomerList'
import SalespersonForm from './SalespersonForm'
import SalespersonList from './SalespersonList'
import SalesList from './SalesList'
import RecordSaleForm from './RecordSaleForm.js'
import SalesPersonHistory from './SalesPersonHistory'

import AppointmentList from './appointmentlist.js'
import AppointmentForm from './AppointmentForm.js'
import TechnicianForm from './TechnicianForm.js'
import TechniciansList from './TechniciansList.js'
import ServiceHistory from './servicehistory.js'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="/models" element={<VehicleModelsList />} />
          <Route path="/models/new" element={<VehicleModelForm />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />

          <Route path="/customers" element={<CustomerList />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/sales_people" element={<SalespersonList />} />
          <Route path="/sales_people/new" element={<SalespersonForm />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sales/new" element={<RecordSaleForm />} />
          <Route path="/sales_person_record" element={<SalesPersonHistory />} />


          <Route path='/appointments' element={<AppointmentList /> } />
          <Route path='/appointments/new' element={<AppointmentForm />} />
          <Route path='/technicians' element={<TechniciansList />} />
          <Route path='/technicians/new' element={<TechnicianForm />} />
          <Route path='/servicehistory' element={<ServiceHistory />} />

        </Routes>
      </div>
    </BrowserRouter>
  )
}

export default App
