import React, { useEffect, useState } from 'react'

// Switched from React class based components to React Functional components with hooks

function CustomerForm() {
    // first name
    const [firstName, setFirstName] = useState('')
    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }
    // last name
    const [lastName, setLastName] = useState('')
    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }
    // address
    const [address, setAddress] = useState('')
    const handleAddressChange = (event) => {
        const value = event.target.value
        setAddress(value)
    }
    // phone number
    const [phoneNumber, setPhoneNumber] = useState('')
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value
        setPhoneNumber(value)
    }
    // submit handler
    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.first_name = firstName
        data.last_name = lastName
        data.address = address
        data.phone_number = phoneNumber

        const customerUrl = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(customerUrl, fetchConfig)
        if (response.ok) {
            setFirstName('')
            setLastName('')
            setAddress('')
            setPhoneNumber('')
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/customers/'
        await fetch(url)
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a New Customer</h1>

                <form onSubmit={handleSubmit} id="create-salesperson-form">

                    <div className="form-floating mb-3">
                        <input onChange={handleFirstNameChange} placeholder="first Name" required type="text" name="first name" id="first name" className="form-control" value={firstName}/>
                        <label htmlFor="first name">First Name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleLastNameChange} placeholder="last name" required type="text" name="last name" id="last name" className="form-control" value={lastName}/>
                        <label htmlFor="last name">Last Name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleAddressChange} placeholder="address" required type="text" name="address" id="address" className="form-control" autoComplete="false" value={address}/>
                        <label htmlFor="address">Address</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handlePhoneNumberChange} placeholder="phone number" required type="text" name="phone number" id="phone number" className="form-control" value={phoneNumber}/>
                        <label htmlFor="phone number">Phone Number</label>
                    </div>

                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
        </div>
    )
}
export default CustomerForm
