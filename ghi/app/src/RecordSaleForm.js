import React, { useState, useEffect } from "react"

function RecordSaleForm(){
    const [automobiles, setAutos] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [customers, setCustomers] = useState([])
    const [formData, setFormData] = useState({
        price: '',
        automobile: '',
        salesperson: '',
        customer: '',
    })

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value,

        })
    }

    const fetchData = async () => {
        const customerurl = 'http://localhost:8090/api/customers/'
        const response = await fetch(customerurl)
        if (response.ok) {
            const data = await response.json()
            setCustomers(data.customers)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const getAutos = async function() {
        const autourl = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(autourl)

        if (response.ok) {
            const automobileList = await response.json()
            const automobile = automobileList.autos
            const unsoldAutomobiles = automobile.filter(autos => autos.sold === false)
            setAutos(unsoldAutomobiles)
        }
    }

    useEffect(() => {
        getAutos()
    }, [])

    const getSalespeople = async function () {
        const Salespeopleurl = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(Salespeopleurl)

        if (response.ok) {
            const data = await response.json()
            setSalespeople(data.salesperson)
        }
    }

    useEffect(() => {
        getSalespeople()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const salesUrl = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(salesUrl, fetchConfig)
        if (response.ok) {
            setFormData({
                price: '',
                automobile: '',
                salesperson: '',
                customer: '',
            })
        }

    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record Sale</h1>
                        <form onSubmit={handleSubmit} id="create-a-record-of-sale-form">
                            <div className="form-floating mb-3">
                                <select onChange={handleChange} required name="automobile" id="automobile" className="form-select" value={formData.automobile}>
                                    <option value="">VIN</option>
                                    {automobiles.map(auto => {
                                        return (
                                            <option key={auto.href} value={auto.href}>{auto.vin}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                            <select onChange={handleChange} required name="salesperson" id="salesperson" className="form-select" value={formData.salesperson}>
                                    <option value=""> Salesperson </option>
                                    {salespeople.map(salesperson => {
                                        return(
                                            <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <select onChange={handleChange} required name="customer" id="customer" className="form-select" value={formData.customer}>
                                    <option value=""> Customer</option>
                                    {customers.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleChange} value={formData.price} placeholder="price" name="price" id="price" className="form-control"/>
                                <label htmlFor="price">Price</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                </div>
            </div>
        </div>
    )
}
export default RecordSaleForm
