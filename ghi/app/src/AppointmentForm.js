import React, { Component } from 'react';

class AppointmentForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: "",
            customer: "",
            date: "",
            time: "",
            technician: "",
            technicians: [],
            reason: "",

        };
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const { vin, customer, date, time, technician, reason, } = this.state;
        const data = { vin, customer, date, time, technician, reason, };

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data),
        };

        fetch(appointmentUrl, fetchConfig)
            .then(response => {
                if (!response.ok) {
                    return response.text().then(text => {
                        throw new Error(text || 'Failed to create appointment');
                    });
                }
                return response.json();
            })
            .then(newAppointment => {
                console.log('New appointment created:', newAppointment);
                this.setState({
                    vin: "",
                    customer: "",
                    date: "",
                    time: "",
                    technician: "",
                    reason: "",

                });
            })
            .catch(error => {
                console.error('Error:', error);
                alert('Error: ' + error.message);
            });
    }

    componentDidMount() {
        this.fetchTechnicians();
    }

    fetchTechnicians = () => {
        fetch('http://localhost:8080/api/technicians/')
            .then(response => response.json())
            .then(data => {
                if (data && Array.isArray(data.technicians)) {
                    this.setState({ technicians: data.technicians });
                } else {
                    console.error('Received data is not in the expected format:', data);
                    this.setState({ technicians: [] });
                }
            })
            .catch(error => {
                console.error('Error fetching technicians:', error);
                this.setState({ technicians: [] });
            });
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card shadow p-4 mt-5">
                            <h1 className="text-center">Create a Service Appointment</h1>
                            <form onSubmit={this.handleSubmit} className="p-3">
                                <div className="form-group mb-3">
                                    <label htmlFor="vin">VIN</label>
                                    <input type="text" name="vin" id="vin" className="form-control" placeholder="VIN" value={this.state.vin} onChange={this.handleChange} required />
                                </div>
                                <div className="form-group mb-3">
                                    <label htmlFor="customer">Customer</label>
                                    <input type="text" name="customer" id="customer" className="form-control" value={this.state.customer} onChange={this.handleChange} required />
                                </div>
                                <div className="form-group mb-3">
                                    <label htmlFor="date">Date</label>
                                    <input type="date" name="date" id="date" className="form-control" placeholder="Date" value={this.state.date} onChange={this.handleChange} required />
                                </div>
                                <div className="form-group mb-3">
                                    <label htmlFor="time">Time</label>
                                    <input type="time" name="time" id="time" className="form-control" placeholder="Time" value={this.state.time} onChange={this.handleChange} required />
                                </div>
                                <div className="form-group mb-3">
                                    <label htmlFor="technician">Technician</label>
                                    <select
                                        name="technician"
                                        id="technician"
                                        className="form-control"
                                        value={this.state.technician}
                                        onChange={this.handleChange}
                                        required
                                    >
                                        <option value="">Select a Technician</option>
                                        {this.state.technicians.map(technician => (
                                            <option key={technician.id} value={technician.id}>
                                                {technician.first_name} {technician.last_name}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                                <div className="form-group mb-3">
                                    <label htmlFor="reason">Reason for appointment</label>
                                    <input type="text" name="reason" id="reason" className="form-control" value={this.state.reason} onChange={this.handleChange} required />
                                </div>
                                <button type="submit" className="btn btn-success">Create Appointment</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AppointmentForm;
