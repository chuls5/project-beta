import React from "react"

// This class represents a React component that will render a list of vehicle models
class VehicleModelsList extends React.Component {
// Initilize component state with an empty array
    constructor(props) {
        super(props)
        this.state = {vehiclesModels: []}
    }
// Make an asynchronus HTTP request to fetch data from the specified URL
// If the response is successful, data is extracted as JSON and used to update the component's state with vehicle models
    async componentDidMount() {
        const response = await fetch('http://localhost:8100/api/models/')
        if (response.ok) {
            const data = await response.json()
            this.setState({ vehiclesModels: data.models })
        }
    }
// Return the JSX that represents the UI of the component
// Map the vehicle data obtained from the component's state to table rows using the 'map' function
    render() {
        return (
            <>
            <h1>Vehicle Models</h1>
            <table className="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.vehiclesModels.map(vehicle => {
                        return (
                            <tr key={vehicle.id}>
                                <td>{vehicle.name}</td>
                                <td>{vehicle.manufacturer.name}</td>
                                <td><img src={vehicle.picture_url} width="200" /></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            </>
        )
    }
}

export default VehicleModelsList
