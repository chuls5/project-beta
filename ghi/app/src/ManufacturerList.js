import React, { useEffect, useState } from "react"

function ManufacturerList() {
	const [manufacturers, setManufacturers] = useState([])

	const getManufacturers = async () => {
		const response = await fetch("http://localhost:8100/api/manufacturers/")

		if (response.ok) {
			const data = await response.json()
			setManufacturers(data.manufacturers)
		}
	}

    useEffect(() => {
        getManufacturers()
    }, [])

	return (
		<div>
			<h1 className="my-3">Manufacturers</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Name</th>
					</tr>
				</thead>
				<tbody>
					{manufacturers.map((manufacturer) => {
						return (
							<tr key={manufacturer.id}>
								<td>{manufacturer.name}</td>
							</tr>
						)
					})}
				</tbody>
			</table>
		</div>
	)
}

export default ManufacturerList
