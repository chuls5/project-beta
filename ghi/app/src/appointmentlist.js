import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function AppointmentList() {
    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        const fetchAppointments = async () => {
            const response = await fetch('http://localhost:8080/api/appointments/');
            if (response.ok) {
                const data = await response.json();
                const dontlist = data.appointments.filter(appointment => appointment.status !== 'canceled' && appointment.status !== 'finished');
                setAppointments(dontlist);
            }
        };

        fetchAppointments();
    }, []);

    const cancelAppointment = async (id) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({ canceled: true }),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        }
    };

    const finishAppointment = async (id) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({ finished: true }),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        }
    };

    let appointmentHistory = (
        <p>No appointments yet</p>
    );

    if (appointments.length > 0) {
        appointmentHistory = (
            <table className="table">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Customer Names</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason</th>
                        <th>Technician</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.technician_id}</td>


                            <td>
                                <button className="btn btn-outline-danger" onClick={() => cancelAppointment(appointment.id)}>Cancel Appointment</button>
                            </td>
                            <td>
                                <button className='btn btn-outline-success' onClick={() => finishAppointment(appointment.id)}>Finished</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        );
    }

    return (
        <div className='container pt-5'>
            <h1>Service Appointments</h1>
            {appointmentHistory}
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-end">
                <Link to="/appointments/new" className="btn btn-success btn-lg px-4 gap-3">Schedule an appointment!</Link>
            </div>
        </div>
    );
}

export default AppointmentList;
