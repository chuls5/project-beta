import React, { useEffect, useState } from 'react';

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const [error, setError] = useState(null);

    useEffect(() => {
        const fetchAppointments = async () => {
            try {
                const response = await fetch('http://localhost:8080/api/appointments/');
                if (response.ok) {
                    const data = await response.json();
                    setAppointments(data.appointments || []);
                } else {
                    setError('Failed to fetch appointments');
                }
            } catch (error) {
                setError('Error fetching appointments');
            }
        };

        fetchAppointments();
    }, []);

    const handleSearchChange = (event) => {
        setSearchQuery(event.target.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
    };

    const filteredAppointments = appointments.filter(appointment =>
        appointment.vin && appointment.vin.toLowerCase().includes(searchQuery.toLowerCase())
    );

    return (
        <div className='container'>
            <h1>Service History</h1>
            <form onSubmit={handleSubmit} className="mb-3">
                <div className="input-group">
                    <input
                        type="text"
                        placeholder="Search by VIN"
                        value={searchQuery}
                        onChange={handleSearchChange}
                        className="form-control"
                    />
                    <button type="submit" className="btn btn-primary">Submit</button>
                </div>
            </form>
            {error ? (
                <p>{error}</p>
            ) : (
                filteredAppointments.length ? (
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Vin</th>
                                <th>Customer Name</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Reason</th>
                                <th>Technician</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredAppointments.map((appointment) => (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.time}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.technician_id}</td>
                                    <td>{appointment.status}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                ) : (
                    <p>No service history available.</p>
                )
            )}
        </div>
    );
}

export default ServiceHistory;
