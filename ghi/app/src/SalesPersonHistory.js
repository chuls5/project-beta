import { useState, useEffect} from 'react'

function SalesPersonHistory() {
  const [sales, setSales] = useState([])
  const [salespeople, setSalespeople] = useState([])
  const [salesperson, setSalesperson] = useState("")

  const handleChange = (event) => {
    const value = event.target.value
    setSalesperson(value)
  }

  const getSales= async () => {
    const salesUrl = 'http://localhost:8090/api/sales/'
    const response = await fetch(salesUrl)
    if (response.ok) {
      const data = await response.json()
      setSales(data.sales)
    }
  }

  const getSalespeople = async () => {
    const salespersonUrl = 'http://localhost:8090/api/salespeople/'
    const response = await fetch(salespersonUrl)
    if (response.ok) {
      const data = await response.json()
      setSalespeople(data.salesperson)
    }
  }

  useEffect(() => {
    getSales()
    getSalespeople()
  }, [])

  return (
    <>
    <h1>Salesperson History</h1>

        <div className="form-floating mb-3">
            <select onChange={handleChange} name="salesperson" id="salesperson" className="form-select" value={salesperson}>
                <option value=""> Choose a Salesperson</option>
                {salespeople?.map(salesperson => {
                    return (
                    <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                    )
                })}
            </select>
        </div>

        <table className="table table-striped">

            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>

            <tbody>
                {sales?.filter((sale) => parseInt(sale.salesperson.employee_id) === parseInt(salesperson)).map(sale =>{
                    return (
                        <tr key={sale.id}>
                            <td>{ sale.salesperson.first_name } {sale.salesperson.last_name}</td>
                            <td>{ sale.customer.first_name } {sale.customer.last_name}</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>{ sale.price }</td>
                        </tr>
                    )
                })}
            </tbody>

        </table>
    </>
    )
}

export default SalesPersonHistory
