import React from "react";

class TechniciansList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            technicians: [],
            error: null,
            isLoading: true
        };
        this.getTechnicians = this.getTechnicians.bind(this);
    }

    getTechnicians() {
        fetch('http://localhost:8080/api/technicians/')
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                console.log('Fetched data:', data);
                this.setState({
                    technicians: data.technicians || data,
                    isLoading: false
                });
            })
            .catch(e => {
                console.error('Error fetching technicians:', e);
                this.setState({
                    technicians: [],
                    error: 'Failed to load technicians',
                    isLoading: false
                });
            });
    }

    componentDidMount() {
        this.getTechnicians();
    }

    render() {
        const { isLoading, error, technicians } = this.state;
        return (
            <>
                <h2 className="display-5 fw-bold">Technicians</h2>
                {error && <div className="alert alert-danger">{error}</div>}
                {isLoading ? (
                    <div>Loading...</div>
                ) : (
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Employee ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {technicians.map(technician => (
                                <tr key={technician.id}>
                                    <td>{technician.employee_id}</td>
                                    <td>{technician.first_name}</td>
                                    <td>{technician.last_name}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                )}
            </>
        );
    }
}

export default TechniciansList;
