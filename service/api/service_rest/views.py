from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO

# Create your views here.
class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "id",
        "employee_id"
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date",
        "time",
        "reason",
        "vin",
        "customer",
        "technician",

    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "technician": TechnicianEncoder(),
    }

    def get_extra_data(self, o):
        date = json.dumps(o.date, default=str)
        time = json.dumps(o.time, default=str)
        date = json.loads(date)
        time = json.loads(time)
        return {
            "date": date,
            "time": time,
        }


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technicians": list(technicians.values())}, encoder=TechnicianEncoder)
    elif request.method == "POST":
        data = json.loads(request.body)
        technician = Technician.objects.create(**data)
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False,)

@require_http_methods(["GET", "DELETE", "PUT"])
def api_delete_technician(request, id):
    technician = get_object_or_404(Technician, id=id)
    if request.method == "GET":
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
    elif request.method == "DELETE":
        technician.delete()
        return JsonResponse({"message": "Technician deleted"})
    elif request.method == "PUT":
        data = json.loads(request.body)
        for key, value in data.items():
            setattr(technician, key, value)
        technician.save()
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse({"appointments": list(appointments.values())}, encoder=AppointmentEncoder)
    elif request.method == "POST":
        data = json.loads(request.body)
        data['technician'] = get_object_or_404(Technician, id=data.get('technician'))
        appointment = Appointment.objects.create(**data)
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_appointment(request, id):
    appointment = get_object_or_404(Appointment, id=id)
    if request.method == "GET":
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
    elif request.method == "DELETE":
        appointment.delete()
        return JsonResponse({"message": "Appointment deleted"})
    elif request.method == "PUT":
        data = json.loads(request.body)
        for key, value in data.items():
            setattr(appointment, key, value)
        appointment.save()
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)

@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    appointment = get_object_or_404(Appointment, id=id)
    appointment.status = 'canceled'
    appointment.save()
    return JsonResponse({"message": "Appointment canceled"}, encoder=AppointmentEncoder, safe=True)

@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    appointment = get_object_or_404(Appointment, id=id)
    appointment.status = 'finished'
    appointment.save()
    return JsonResponse({"message": "Appointment finished"}, encoder=AppointmentEncoder, safe=True)
